<?php

$nome = !empty($_POST['nome']) ? $_POST['nome'] : '';
$telefone = !empty($_POST['telefone']) ? $_POST['telefone'] : '';
$email = !empty($_POST['email']) ? $_POST['email'] : die('Por favor, preencha o seu e-mail.');
$mensagem = !empty($_POST['mensagem']) ? $_POST['mensagem'] : die('Por favor, preencha a mensagem.');

$from = 'decormax@decormaxambientes.com.br';

$headers  = "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=utf-8\r\n";
$headers .= "From: {$from}\r\n";
$headers .= "Reply-To: {$email}\r\n";
$headers .= "Return-Path: {$from}\r\n";
$body  = "<style>";
$body .= "table { width: 100%; font: normal 13px/20px Arial, sans-serif; }";
$body .= "th, td { padding: 4px 8px; border-bottom: 1px solid #ddd; }";
$body .= "th { width: 140px; text-align: left; }";
$body .= "</style>";
$body .= "<table>";
$body .= "<tr>";
$body .= "<th>Nome:</th>";
$body .= "<td>{$nome}</td>";
$body .= "</tr><tr>";
$body .= "<th>Telefone:</th>";
$body .= "<td>{$telefone}</td>";
$body .= "</tr><tr>";
$body .= "<th>E-mail:</th>";
$body .= "<td>{$email}</td>";
$body .= "</tr><tr>";
$body .= "<th>Mensagem:</th>";
$body .= "<td>{$mensagem}</td>";
$body .= "</tr>";
$body .= "</table>";

mail($from, 'Site > Contato', $body, $headers, "-r" . $from);
