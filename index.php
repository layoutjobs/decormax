<?php include('functions.php'); ?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Decormax Ambientes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Localizada em Salto/SP, a Decormax se dedica para oferecer a excelência em seus produtos e processos. Projetando, planejando e construindo móveis com qualidade em todos os detalhes. O controle de qualidade é um diferencial a parte, feito peça por peça, garantindo um produto impecável.">
    <meta name="keywords" content="móveis planejados, moveis planejados, móvel planejado, movel planejado, ambientes, planejados, decoração, design, móveis fabricação própria, grupo zanini">
    <meta name="author" content="Layout - Criação e Publicidade - Salto/SP">

    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300,200" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet" type="text/css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
    <![endif]-->
  </head>

  <body id="home" data-spy="scroll" data-target="#mainnav">


    <!-- TOP
    ================================================== -->


    <section id="top">
      <ul>
        <li class="item1"><img src="assets/img/topo-01.jpg" /></li>
        <li class="item2"><img src="assets/img/topo-02.jpg" /></li>
        <li class="item3"><img src="assets/img/topo-03.jpg" /></li>
      </ul>
      <p class="kaushan"><span>Sua casa aconchegante, confortável e sofisticada, como você sempre quis.</span></p>
    </section>


    <!-- HEADER
    ================================================== -->


    <header id="header" class="block">
      <div class="container">
        <div class="row">
          <div class="span4"><a href="#" class="brand"><img src="assets/img/logo.png" alt="Decormax" /></a></div>
          <div class="span8">
            <nav id="mainnav">
              <ul class="nav">
                <li class="active"><a href="#home">Home</a></li>
                <li><a href="#empresa">Quem Somos</a></li>
                <li><a href="#portfolio">Móveis Planejados</a></li>
                <li><a href="#contato">Contato</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>


    <!-- BANNER
    ================================================== -->


    <section id="banner" class="block inverse text-center">
      <div class="block-flush">
        <div class="container">
          <h1>Qualidade é o nosso diferencial</h1>
          <p class="lead">A qualidade Decormax envolve um conjunto de fatores. Começa com a escolha da madeira ecologicamente correta, o uso de equipamentos com a mais moderna tecnologia, o emprego de mão-de-obra altamente especializada, a utilização dos melhores materiais de acabamento e o constante monitoramento da excelência na produção. Com isso a Decormax garante a alta qualidade de seus produtos bem como a satisfação de seus clientes.</p>
        </div>
      </div>
    </section>


    <!-- EMPRESA
    ================================================== -->


    <section id="empresa" class="block inverse">
      <div class="container">
        <p class="kaushan">Modernidade, sofisticação e qualidade. Nosso mix de produtos propõe a solução ideal e sob medida para grandes e pequenos ambientes, gerando economia e praticidade para você.</p>
        <div class="br br-large"></div>
        <div class="row">
          <div class="span6 col1">
            <h1>A Decormax</h1>
            <div class="br br-mini"></div>
            <p>A Decormax se dedica para oferecer a excelência em seus produtos e processos. Projetando, planejando e construindo móveis com qualidade em todos os detalhes. O controle de qualidade é um diferencial a parte, feito peça por peça, garantindo um produto impecável.</p>
            <p>A Decormax possuí fabricação própria, por esse motivo qualquer ajuste na execução do projeto é realizado com agilidade e qualidade, proporcionando um produto final personalizado e perfeito.</p>
            <p>Traga suas idéias e o seu projeto será desenhado sem compromisso, planejado para aproveitar todo o espaço da sua casa, proporcionando maior conforto, durabilidade e praticidade com a garantia de fabricação Decormax. Fale conosco.</p>
            <div class="br"></div>
          </div>
          <div class="span6 col2"><img src="assets/img/planta-3d.png" /></div>
        </div>
      </div>
    </section>


    <!-- PORTFÓLIO
    ================================================== -->


    <section id="portfolio" class="block">
      <div class="block-flush">
        <div class="container">
          <h1>Decormax, ambientes com modernidade, sofisticação e qualidade.</h1>
          <div class="view">
            <div class="br-small"></div>
            <div id="portfolio-carousel" class="carousel slide">
              <div class="carousel-inner">
                <?php $jobs = getJobs(); ?>
                <?php $counter = 0; ?>
                <?php foreach ($jobs as $item) : ?>
                <?php foreach ($item as $row) : ?>
                <?php foreach ($row as $job) : ?>
                <?php $counter++; ?>
                <div class="item<?php echo $counter === 1 ? ' active' : ''; ?>">
                  <img src="<?php echo $job['imgs']['large']['url']; ?>" alt="<?php echo $job['name']; ?>" />
                </div>
                <?php endforeach; ?>
                <?php endforeach; ?>
                <?php endforeach; ?>
              </div>
              <a class="left carousel-control" href="#portfolio-carousel" data-slide="prev">&lsaquo;</a>
              <a class="right carousel-control" href="#portfolio-carousel" data-slide="next">&rsaquo;</a>
            </div>
            <img class="casal" src="assets/img/casal.png" />
            <div class="br"></div>
          </div>
        </div>
        <div class="block-flush thumbs">
          <div class="container">
            <div id="portfolio-carousel-thumbs" class="carousel slide">
              <div class="carousel-inner">
                <?php $jobs = getJobs(); ?>
                <?php $counter = 0; ?>
                <?php foreach ($jobs as $item) : ?>
                <?php foreach ($item as $row) : ?>
                <div class="item<?php echo $counter === 0 ? ' active' : ''; ?>">
                  <ul>
                    <?php foreach ($row as $job) : ?>
                    <?php $counter++; ?>
                    <li>
                      <a href="#" data-target="#portfolio-carousel" data-slide-to="<?php echo $counter - 1; ?>">
                         <img src="<?php echo $job['imgs']['large']['url']; ?>" alt="<?php echo $job['name']; ?>" />
                      </a>
                    </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
                <?php endforeach; ?>
                <?php endforeach; ?>
              </div>
              <a class="left carousel-control" href="#portfolio-carousel-thumbs" data-slide="prev">&lsaquo;</a>
              <a class="right carousel-control" href="#portfolio-carousel-thumbs" data-slide="next">&rsaquo;</a>
            </div>
          </div>
        </div>
      </div>
    </section>


    <!-- CONTATO
    ================================================== -->


    <footer id="contato" class="block">
      <div class="br"></div>
      <div class="container">
        <div class="row">
          <div class="span8">
            <address>
              <span class="lead phone"><i></i><strong>11 4456.5541</strong></span>
              <span class="address"><i></i>Av. José Marques de Oliveira, 384 C · Vila Norma · Salto/SP</span>
            </address>
          </div>
          <hr class="visible-phone" />
          <div class="span4">
            <span class="grupo-zanini lead"><a href="http://grupozaninidecor.com.br/">Uma empresa do <br /><strong>Grupo Zanini Decor</strong></a></span>
          </div>
        </div>
      </div>
      <div class="br"></div>
      <div class="block-flush">
        <div class="container">
          <h1>Fale conosco</h1>
          <p class="lead">Nossa maior especialidade é atendê-lo sempre da melhor forma.</p>
          <div id="contato-mapa" class="map"></div>
          <form id="contato-form" action="contato.php" method="post">
            <input type="text" name="nome" placeholder="Nome" required="required" class="input-block-level" />
            <input type="text" name="telefone" placeholder="Telefone" class="input-block-level" />
            <input type="email" name="email" placeholder="E-mail" required="required" class="input-block-level" />
            <textarea name="mensagem" placeholder="Como podemos ajudar?" required="required" class="input-block-level"></textarea>
            <button type="submit" class="btn btn-inverse input-block-level">Enviar Mensagem</button>
          </form>
          <div class="br"></div>
          <small class="rights">Copyright &copy;<?php echo date('Y'); ?> Decormax. Todos os direitos reservados.</small>
        </div>
      </div>
    </footer>


    <!-- FLASH
    ================================================== -->


    <div id="flash" class="hide"></div>


    <!-- SCRIPTS
    ================================================== -->


    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/validate.js"></script>
    <script src="assets/js/placeholder.js"></script>


    <!-- GOOGLE ANALYTICS TRACKING CODE
    ================================================== -->


    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-45863718-1', 'www.decormaxambientes.com.br');
      ga('send', 'pageview');

    </script>

  </body>
</html>
