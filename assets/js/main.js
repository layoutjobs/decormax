!function ($) {

  $(function () {

    var $window   = $(window)
    var $flash    = $('#flash')
    var $body     = $('body')
    var offsetTop = 0 // modificado em sizing()

    $(window).resize(function () { 
      sizing() 
    })

    $(document).ready(function () { 
      sizing() 
    }) 

    $(document).scroll( function () {
      setFixed()     
    })

    // Incicializa o plugin placeholder.js.
    $.Placeholder.init()

    // Dimensionamento do layout.
    function sizing () {
      if ($window.height() <= (660 + getHeaderHeight()) && $window.width() >= 768)
        offsetTop = $window.height() - getHeaderHeight()
      else {
        if ($window.width() >= 768)
          offsetTop = 660
        else
          offsetTop = 0
      }
      $('#top').height(offsetTop)
      setFixed()
      matchColsHeight()   
    }

    // Possibilita a animação via CSS 3 dos elementos de topo. 
    $('#top ul > li').on('mouseenter', function () {
      var $this = this
      var $parent = $($this).parents('ul')
      var $items = $parent.children('li')
      var index = $items.index($this) + 1
      
      $parent.removeClass().addClass('in-item' + index  )
      $parent.bind('mouseleave', function () {
        $(this).removeClass()
      })
    })

    // Define se o layout é fixo ou não.
    function setFixed () {
      if ($window.width() >= 480 && $window.scrollTop() >= offsetTop) {
        $body.addClass('fixed')
        $body.css('padding-top', offsetTop + getHeaderHeight())
        $('#header').css('margin-top', 0)  
      } else {
        $body.removeClass('fixed') 
        $body.css('padding-top', 0)   
        $('#header').css('margin-top', offsetTop)
      }
    }


    // Atribui o efeito de âncora aos evento click de alguns elementos.
    $('#header .nav > li > a, .brand').on('click', function (e) { 
      arcor(this) 
      e.preventDefault()
    })


    // Obtem a altura do #header dinâmicamente.
    function getHeaderHeight () {
      return $('#header').outerHeight()
    } 


    // Corresponde a altura das colunas. Aplicável somente a .row > .col2.
    function matchColsHeight () {
      if ($window.width() >= 768) {
        $('.row > .col2').css('min-height', 0)
        $('.row > .col2').css('min-height', function () {
          return $(this).parents('.row').outerHeight() + 'px'
        })
      } else {
        $('.row > .col2').css('min-height', 0)        
      }      
    }


    // Âncora com efeito animado.
    function arcor (object) {
      var href = $(object).attr('href')

      $('html, body').animate({
        scrollTop: href != '#' ? $(href).offset().top - getHeaderHeight() : 0
      })      
    }

    $.extend($.validator.messages, {
          required: 'Por favor, preencha este campo.'
        , email: 'Por favor, informe um e-mail válido.'
    });

    // Habilita a validação do #contato-form.
    $('#contato-form').validate()

    // Envia o #contato-form via ajax.
    $('#contato-form').submit(function (e) {
      var $this = $(this)

      if ($this.valid()) {
        $.ajax({
            type: $this.attr('method')
          , url: $this.attr('action')
          , dataType: 'html'
          , data: $this.serialize()
          , beforeSend: function () { $this.addClass('loading') }
          , complete: function () { $this.removeClass('loading') }
        }).done(function (data) {
          if (data.length) {
            $flash
              .html('<i class="icon-remove icon-white"></i> ' + data)
              .removeClass('hide')
              setTimeout(function() {
                $flash.addClass('hide') 
              }, 6000)
          } else {
            // $this.reset()
            $flash
              .html('<i class="icon-ok icon-white"></i> Mensagem enviada com sucesso. Obrigado pelo contato.')
              .removeClass('hide')
              setTimeout(function() {
                $flash.addClass('hide') 
              }, 6000)
          }
        })
      }
      return false
    })

  })

}(window.jQuery)