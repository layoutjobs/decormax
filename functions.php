<?php

function getJobs()
{
	$baseDir = dirname(__FILE__) . '/files/jobs/';
	$baseUrl = 'files/jobs/';

	$folders = glob($baseDir . '*', GLOB_ONLYDIR);

	foreach ($folders as $i => $folder) {
		$folders[$i] = str_replace('/', '', str_replace(dirname($folder), '', $folder));
	}

	foreach ($folders as $folder) {
		$files = glob($baseDir . $folder . '/*');
		foreach ($files as $file) {
			if (!is_dir($file) && preg_match('/(\.|\/)(gif|jpe?g|png|txt)$/i', $file)) {
				$fileName = str_replace('/', '', str_replace(dirname($file), '', $file));
				$extension = strrchr($file, '.');
				$fileNameWithoutExtension = str_replace($extension, '', $fileName);

				if ($fileName === 'info.txt')
					$info = explode("\n", utf8_encode(file_get_contents($file, true)));

 				$imgs[$fileNameWithoutExtension] = array(
					'name' => $fileName,
					'extension' => $extension,
					'url' => $baseUrl . $folder . '/' . $fileName
				);
			}
		}

		$jobs[$folder] = array(
			'name' => !empty($info[0]) ? $info[0] : '',
			'description' => !empty($info[1]) ? $info[1] : '',
			'imgs' => $imgs
		); 
	}

	return !empty($jobs) ? array_chunk(array_chunk($jobs, 4), 4) : array();
}